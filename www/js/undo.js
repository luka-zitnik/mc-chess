(function() {
    var observer = new MutationObserver(function(mutations, observer) {
        mutations.forEach(function(mutation) {
            undo.stack.push(mutation);
        });
    });

    // Undoes removed/added nodes from its internal array of MutationRecords
    var undo = {

        // An array of MutationRecords
        // They are probably safe only up until garbage collection kicks in
        stack: [],

        undoMicroMutation: function() {
            var step = this.stack.pop(),
                removedNodesCount,
                addedNodesCount,
                i,
                j;

            if (step === undefined) {
                return;
            }

            removedNodesCount = step.removedNodes.length;
            addedNodesCount = step.addedNodes.length;
            i = 0;
            j = 0;

            observer.disconnect();

            for (; i < removedNodesCount; ++i) {
                step.target.appendChild(step.removedNodes[i]);
            }

            for (; j < addedNodesCount; ++j) {
                step.target.removeChild(step.addedNodes[j]);
            }

            observer.connect();
        },

        undo: function() {
            this.undoMicroMutation(); // For the added piece
            this.undoMicroMutation(); // For the removed piece
        }
    };

    document.getElementById("undo").onclick = function() {
        undo.undo();
    };

    observer.connect = function() {
        observer.observe(document.querySelector(".board"), {
            childList: true,
            subtree: true
        });
    };

    observer.connect();
})();