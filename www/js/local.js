(function () {
    var pieces = document.querySelectorAll(".piece");
    var boardCells = document.querySelectorAll(".square.dark, .square.light");

    function onDragStart(event) {
        event.dataTransfer.effectAllowed = "move";
        event.dataTransfer.setData("text/plain", event.target.value);
        event.dataTransfer.setData("text/html", event.target.outerHTML);
    }

    for (var i in pieces) {
        pieces[i].ondragstart = onDragStart;
    }

    for (var j in boardCells) {
        boardCells[j].ondragenter = function (event) {
            event.preventDefault();
        };
        boardCells[j].ondragover = function (event) {
            event.preventDefault();
        };
        boardCells[j].ondrop = function (event) {
            event.preventDefault();

            this.innerHTML = event.dataTransfer.getData("text/html");
            this.ondragstart = onDragStart;
        };
        boardCells[j].ondragend = function (event) {
            if (event.dataTransfer.dropEffect !== "move") {
                return;
            }

            this.removeChild(event.target);
        };
    }
})();