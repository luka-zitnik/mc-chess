(function() {
    var board = document.querySelector(".board");

    function setConnectionStatus(className) {
        document.getElementById("connection-status").className = className;
    }

    function driveMoves(moves) {
        var i = 0,
            move;

        for (; i < moves.length; ++i) {
            move = moves[i];

            if (move.oldPosition) {
                board
                    .children[move.oldPosition.rank]
                    .children[move.oldPosition.file]
                    .innerHTML = "";
            }

            if (move.newPosition) {
                board
                    .children[move.newPosition.rank]
                    .children[move.newPosition.file]
                    .innerHTML = move.piece;
            }
        }
    }

    function createObserver(connection) {
        var observer;

        function getSquarePosition(square) {
            return {
                file: Array.prototype.indexOf.call(square.parentNode.children, square),
                rank: Array.prototype.indexOf.call(square.parentNode.parentNode.children, square.parentNode)
            };
        }

        observer = new MutationObserver(function(mutations, observer) {
            var moves;

            if (typeof connection.send !== "function") {
                return;
            }

            moves = [];

            mutations.forEach(function(mutation) {
                var removedNodesCount = mutation.removedNodes.length,
                    addedNodesCount = mutation.addedNodes.length,
                    squarePosition = getSquarePosition(mutation.target),
                    i = 0,
                    j = 0;

                for(; i < removedNodesCount; ++i) {
                    moves.push({
                        piece: mutation.removedNodes[i].outerHTML,
                        oldPosition: squarePosition,
                        newPosition: null
                    });
                }

                for(; j < addedNodesCount; ++j) {
                    moves.push({
                        piece: mutation.addedNodes[j].outerHTML,
                        oldPosition: null,
                        newPosition: squarePosition
                    });
                }
            });

            connection.send(moves);
            console.debug("Sent", moves);
        });

        observer.connect = function() {
            observer.observe(board, {
                childList: true,
                subtree: true
            });
        };

        observer.connect();

        return observer;
    }

    function initializeConnection(onopen) {
        var peer = new Peer({
            key: "v4fjypnyo6layvi",
            config: { iceServers: [
                // Googles public STUN server
                // https://code.google.com/p/natvpn/source/browse/trunk/stun_server_list
                {url: "stun:stun.l.google.com:19302"},

                // A public TURN server
                // http://numb.viagenie.ca/
                {url: "turn:numb.viagenie.ca", credential: "sd123HO@40rAJehfg2403", username: "luka.zitnik@gmail.com"}
            ]},
            debug: 0
        });

        peer.on("open", function(id) {
            document.getElementById("own-id").textContent = id;
        });

        peer.on("error", function(error) {
            console.error(error);
        });

        peer.on("connection", function(conn) {
            conn.on("open", onopen);
        });

        document.getElementById("invite").onclick = function() {
            peer.connect(document.getElementById("peers-id").value)
                .on("open", onopen);
        };
    }

    initializeConnection(function () {
        var observer = createObserver(this);
        setConnectionStatus("conn-open");

        this.on("data", function(data) {
            observer.disconnect();
            driveMoves(data);
            console.debug("Received", data);
            observer.connect();
        });
    });
})();